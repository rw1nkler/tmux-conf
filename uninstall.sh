#!/bin/bash

# Options
SHOW_HELP=flase
VERBOSE=false

# Configuration variables
TMUX_TARGET=~/.tmux.conf

while getopts ":hv" opt; do
    case ${opt} in
        h )
            SHOW_HELP=true
            ;;
        v )
            VERBOSE=true
            ;;
        \? )
            echo "Invalid option -${OPTARG}"
            exit 1
            ;;
    esac
done

if $VERBOSE; then
    exec 3>&1
else
    exec 3>/dev/null
fi

# Remove tmux symlink

if [[ -h $TMUX_TARGET ]]; then
    echo "INFO: Removing ${TMUX_TARGET}" >&3
    rm -rf ${TMUX_TARGET}
fi

