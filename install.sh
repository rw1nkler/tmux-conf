#!/bin/bash

# Options
SHOW_HELP=false
FORCE=false
VERBOSE=false

# Configuration options
TMUX_SOURCE=`pwd`/tmux.conf
TMUX_TARGET=~/.tmux.conf
TMUX_TPM_DIR=~/.tmux/plugins/tpm

while getopts ":hfv" opt; do
    case ${opt} in
        h )
            SHOW_HELP=true
            ;;
        v )
            VERBOSE=true
            ;;
        f )
            FORCE=true
            ;;
        \? )
            echo "Invalid option -${OPTARG}"
            exit 1
            ;;
    esac
done

if $VERBOSE; then
    exec 3>&1
else
    exec 3>/dev/null
fi

if [[ -d $TMUX_TPM_DIR ]]; then
    echo "INFO: $TMUX_TPM_DIR directory already exists!" >&3
    if $FORCE; then
        echo "INFO: (FORCE) Reclonning tmux tpm repository" >&3
        rm -rf $TMUX_TPM_DIR
        git clone --force git clone https://github.com/tmux-plugins/tpm ${TMUX_TPM_DIR}
    fi
else
    echo "INFO: Clonning tmux tpm repository" >&3
    git clone https://github.com/tmux-plugins/tpm ${TMUX_TPM_DIR}
fi

if ! $FORCE; then
    if [[ -f $TMUX_TARGET ]] && [[ ! -h $TMUX_TARGET ]]; then
        echo "INFO: File ${TMUX_TARGET} exists! Moving to ${TMUX_TARGET}.bak..."
        mv ${TMUX_TARGET} ${TMUX_TARGET}.bak
    fi

    if [[ -h $TMUX_TARGET ]]; then
        LINK_TARGET=`readlink $TMUX_TARGET`
        echo "INFO: Symbolic link to ${LINK_TARGET} exists! Moving to ${TMUX_TARGET}.bak..."
        mv ${TMUX_TARGET} ${TMUX_TARGET}.bak
    fi

    echo "INFO: Creating link to $TMUX_TARGET" >&3
    ln -s ${TMUX_SOURCE} ${TMUX_TARGET}
else
    echo "INFO: (FORCE) Creating link to $TMUX_TARGET" >&3
    ln -f -s ${TMUX_SOURCE} ${TMUX_TARGET}
fi

